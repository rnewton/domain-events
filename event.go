// Package events provides interfaces for publishing and handling domain events in Go.
// In Domain Driven Design, a domain event is a way of indicating that some event has
// happened that has relevance to the domain.
package events

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Event is a generic wrapper for all domain events
type Event struct {
	UUID      string      `json:"uuid"`
	CreatedAt time.Time   `json:"created_at"`
	Name      string      `json:"name"`
	Data      interface{} `json:"data"`
}

// NewEvent creates a new event with the given name and payload. The UUID and CreatedAt fields
// are filled in automatically so that the infrastructure concerns of doing so are removed from
// the domain
func NewEvent(name string, payload interface{}) *Event {
	return &Event{
		UUID:      uuid.NewV4().String(),
		CreatedAt: time.Now(),
		Name:      name,
		Data:      payload,
	}
}
