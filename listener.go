package events

// EventHandler defines the function signature required for handling events
type EventHandler func(*Event) error

// ErrorHandler defines the function signature required for handling errors
type ErrorHandler func(error)

// Listener defines the interface required for listening for new events and handling them
type Listener interface {
	// RegisterHandler registers an event handler for events of the given string name
	RegisterHandler(string, EventHandler)

	// Listen spawns a non-blocking goroutine to listen for new events
	Listen(ErrorHandler)

	// Halt stops the listen goroutine
	Halt()
}
