package events

// Publisher describes the interface for pushing events
type Publisher interface {
	// Publish pushes out the given event to the configured messaging interface
	Publish(*Event) error
}
