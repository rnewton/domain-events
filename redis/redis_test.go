package redis

import (
	"strings"
	"testing"

	uuid "github.com/satori/go.uuid"

	events "gitlab.com/rnewton/domain-events"

	redis "gopkg.in/redis.v3"
)

func redisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
}

func TestSuccessfulEventHandling(t *testing.T) {
	client := redisClient()
	pub := NewPublisher(client, "test_queue")
	sub := NewListener(client, "test_queue")

	sub.RegisterHandler("TestEvent", func(event *events.Event) error {
		t.Logf("Hello from event %s\n", event.UUID)
		return nil
	})
	sub.RegisterHandler("TestEvent", func(event *events.Event) error {
		t.Logf("Hello from event %s again\n", event.UUID)
		return nil
	})

	sub.Listen(func(err error) {
		t.Fatal("Listening should be successful", err)
	})
	if err := pub.Publish(&events.Event{UUID: uuid.NewV4().String(), Name: "TestEvent"}); err != nil {
		t.Fatal("Publishing a test event should be successful", err)
	}
}

func TestNewPublisherFails(t *testing.T) {
	pub := NewPublisher(redisClient(), "test_queue")
	if err := pub.Publish(nil); err == nil {
		t.Fatal("Publisher should fail when provided with invalid event")
	}

	if err := pub.Publish(&events.Event{UUID: "test", Name: "Test", Data: make(chan bool)}); err == nil {
		t.Fatal("Publisher should fail when provided with unencodeable event")
	}

	pub = NewPublisher(redis.NewClient(&redis.Options{Addr: "99.99.99.99:123"}), "test_queue")
	if err := pub.Publish(&events.Event{UUID: "test", Name: "Test"}); err == nil {
		t.Fatal("Publisher should fail when provided with invalid client")
	}
}

func TestListenerHalt(t *testing.T) {
	sub := NewListener(redisClient(), "test_queue")

	sub.Listen(func(err error) {
		if !strings.Contains(err.Error(), "halt") {
			t.Fatalf("Listener should have been halted. Got error '%s' instead", err.Error())
		}
	})

	sub.Halt()
}
