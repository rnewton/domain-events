package redis

import (
	"encoding/json"
	"errors"
	"log"
	"time"

	events "gitlab.com/rnewton/domain-events"
	redis "gopkg.in/redis.v3"
)

// Listener waits for events in a Redis queue and hands them off to processors
type Listener struct {
	redis          *redis.Client
	queueName      string
	deadletterName string
	handlers       map[string][]events.EventHandler
	errors         chan error
}

// NewPublisher returns a new Publisher for the given redis connection and queue name
func NewListener(client *redis.Client, queueName string) *Listener {
	return &Listener{
		redis:          client,
		queueName:      queueName,
		deadletterName: queueName + "_deadletter",
		handlers:       map[string][]events.EventHandler{},
		errors:         make(chan error, 1), // Buffer so that calls to Halt() aren't blocking
	}
}

// RegisterHandler registers an event handler for events of the given string name
func (l *Listener) RegisterHandler(name string, fn events.EventHandler) {
	l.handlers[name] = append(l.handlers[name], fn)
}

// Listen spawns a non-blocking goroutine to listen for new events
func (l *Listener) Listen(errFn events.ErrorHandler) {
	go func() {
		for {
			select {
			case err := <-l.errors:
				errFn(err)
				return
			default:
				rs, err := l.redis.BRPopLPush(l.queueName, l.deadletterName, 30*time.Second).Result()
				if err != nil {
					log.Println("An error occurred while retrieving an event: ", err)
					l.errors <- err
				}

				event := &events.Event{}
				if err := json.Unmarshal([]byte(rs), event); err != nil {
					log.Println("Failed to parse event: ", err)
					l.errors <- err
				}

				log.Printf("Received new event %s - %s with payload %+v\n", event.UUID, event.Name, event.Data)
				l.handleEvent(event, rs)
			}
		}
	}()
}

// handleEvent passes an event to all registered handlers
func (l *Listener) handleEvent(event *events.Event, eventData string) {
	// Pass the event to all registered handlers
	for _, handler := range l.handlers[event.Name] {
		if err := handler(event); err != nil {
			log.Println("Handler failed to process event", err)
			l.errors <- err
		}
	}

	// Remove it from the deadletter queue as we've successfully processed it
	if err := l.redis.LRem(l.deadletterName, 1, eventData).Err(); err != nil {
		log.Println("Failed to remove event from deadletter", err)
		l.errors <- err
	}
}

// Halt stops the listen goroutine
func (l *Listener) Halt() {
	l.errors <- errors.New("Listener halted.")
}
