package redis

import (
	"encoding/json"
	"fmt"

	events "gitlab.com/rnewton/domain-events"
	redis "gopkg.in/redis.v3"
)

// Publisher pushes events to a Redis circular queue
type Publisher struct {
	redis     *redis.Client
	queueName string
}

// NewPublisher returns a new Publisher for the given redis connection and queue name
func NewPublisher(client *redis.Client, queueName string) *Publisher {
	return &Publisher{
		redis:     client,
		queueName: queueName,
	}
}

// Publish pushes out the given event to a Redis circular queue
func (p *Publisher) Publish(event *events.Event) error {
	if event == nil || len(event.UUID) == 0 || len(event.Name) == 0 {
		return fmt.Errorf("Cannot publish invalid event: %+v", event)
	}

	data, err := json.Marshal(event)
	if err != nil {
		return err
	}

	if err := p.redis.LPush(p.queueName, string(data)).Err(); err != nil {
		return err
	}

	return nil
}
