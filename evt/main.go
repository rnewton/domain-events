package main

import "gitlab.com/rnewton/domain-events/evt/cmd"

func main() {
	cmd.Execute()
}
