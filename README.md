# Domain Events

[![GoDoc](https://godoc.org/gitlab.com/rnewton/domain-events?status.svg)](https://godoc.org/gitlab.com/rnewton/domain-events)

This is a package with several implementations for handling Domain events. In [Domain Driven Design](http://www.martinfowler.com/eaaDev/DomainEvent.html), a domain event is a way of indicating that some event has happened that has relevance to the domain.

This specific package provides the interfaces for interacting with these events as a part of a larger application.

## Installation

`go get gitlab.com/rnewton/domain-events`

## Usage

This library is designed to take advantage of a Four-layered Architecture. The specific publishers/subscribers in-use are a infrastructure concern. These are injected into the application layer as dependencies and the domain layer handles the hooks for when events are triggered.

## Hooks

In order to have your entity trigger a domain event automatically, you need to implement the appropriate interface as below:

- `BeforeCreate() *events.Event`
- `AfterCreate() *events.Event`
- `BeforeUpdate() *events.Event`
- `AfterUpdate() *events.Event`
- `BeforeDelete() *events.Event`
- `AfterDelete() *events.Event`

It'll look something like:

```go
package domain

import (
    "time"

    "gitlab.com/rnewton/domain-events"
)

type Something struct {
    All    string
    Sorts  string
    Of     string
    Params string
}

func (s *Something) AfterUpdate() *events.Event {
    return &events.Event{
        UUID:      "Something of significance",
        CreatedAt: time.Now(),
        Name:      "SomethingUpdated",
        Data:      map[string]string{
            "anything": "that we want to capture",
            "can":      "be reflected in this because",
            "type":     "is interface{}"
        },
    }
}
```

Then generate the repository using the `evt` tool.

```
evt -e domain/model/something/something.go -r infrastructure/persistence/db/something_repository_gen.go
```

It'll create a `SomethingRepository` for you that automatically hooks up the change hooks. In your service locator, instantiate the repository you generated:

```go
func (c *Container) SomethingRepository() *db.SomethingRepository {
    if c.somethingRepo == nil {
        c.somethingRepo := &db.SomethingRepository{
            DB:  c.Database(),
            Pub: c.DomainEventsPublisher(),
        }
    }

    return c.somethingRepo
}
```

And when you call `SomethingRepository.Save(*Something)`, it'll automatically call `*Something.AfterUpdate` for you. It does this _without_ reflection because you've already generated the code. This means it's very quick!

The generated file is overwritten every time you run `evt` so it shouldn't be modified. If you need your repository to have custom behavior, create another file that implements the new methods you need. The generated repository is exported and can be extended with new `func (r *SomethingRepository) YourMethod` definitions.

Listen for the events elsewhere. Say a background job cli:

```go
package main

import (
    "fmt"
    "infrastructure"

    "gitlab.com/rnewton/domain-events"
)

func handleSomethingUpdated(event *events.Event) error {
    fmt.Println("It's all good!")
    return nil
}

func main() {
    ...
    listener := container.DomainEventsListener()
    listener.RegisterHandler("SomethingUpdated", handleSomethingUpdated)

    listener.Listen()
}
```
