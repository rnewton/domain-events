package events

// BeforeCreateHook is used for entities to trigger events before being created
type BeforeCreateHook interface {
	BeforeCreate() *Event
}

// AfterCreateHook is used for entities to trigger events after being created
type AfterCreateHook interface {
	AfterCreate() *Event
}

// BeforeUpdateHook is used for entities to trigger events before being updated
type BeforeUpdateHook interface {
	BeforeUpdate() *Event
}

// AfterUpdateHook is used for entities to trigger events after being updated
type AfterUpdateHook interface {
	AfterUpdate() *Event
}

// BeforeDeleteHook is used for entities to trigger events before being deleted
type BeforeDeleteHook interface {
	BeforeDelete() *Event
}

// AfterDeleteHook is used for entities to trigger events after being deleted
type AfterDeleteHook interface {
	AfterDelete() *Event
}
